import pandas as pd
from webbot import Browser
import pandas as pd
from bs4 import BeautifulSoup
from urllib.request import urlopen
import requests
import html2text
import re
from time import sleep


#Finding contact URL function using webbot
def find_contact_page(link):
    print("---------------------------------------------------------------------------")
    #using domain and converting to URL
    link_complete = "http://www." + str(link) 
    contact_url = 'Not_Found'
    
    #opening the URL
    web.go_to(link_complete)
    
    #checking if contact button exists
    if(web.exists(text='contact')):
        #clicking contact button on the URL
        web.click('contact')
        
        #Getting the current URL on the webbot browser
        contact_url = web.get_current_url()
        print(contact_url)
        
    #returning the contact URL
    return(contact_url)


#Function to scrape the contents from contact URL page
def get_content(link):
    print("*******************************************************")
    print(link)
    Adress = 'Not_Found'
    
    #Avoiding problematic URL
    item = ["Not_Found", "https://www.fiat.com.ar/contactenos","https://www.omv.bg/bg-bg"]
    if(link not in item):
        try:
            #Requesting the contents of the contact URL page
            page = requests.get(link)
            
            #printing the status code of the http request
            print(page.status_code)
            
            #parsing the contents using beautifulsoup
            soup = BeautifulSoup(page.content, 'html.parser')
            txt = str(soup)
            
            #Converting HTML to text
            content = html2text.html2text(txt)
            
            #Finding all the location in the contents which have @
            counter = [m.start() for m in re.finditer('@', content)]
            
            #default adress value
            Adress = ' '
            
            #looping into contents which have @ symbol
            for i in counter:
                #-1 is returned when @ is not found
                if(i != -1):
                    #striping the contents short into 200 length
                    temp = content[i-100:i+100]
                    #merging into Adress
                    Adress = Adress + temp
                    #Other strings to check inside the contents
                    substrings = ['Tel','Contact','contact','Email','email','Phone','phone','PH','Address','CONTACT']
                    
                    #checking if the substrings exist in the string
                    check = any([substring in temp for substring in substrings])
                    
                    #Not checking further if the substring exist in the string
                    if(check == True):
                        break
                        
        #Handling all the exceptions
        except requests.exceptions.TooManyRedirects:
            print("too many")
        except requests.exceptions.RequestException as e:
            print(e)
    
    #returning the address
    return(Adress)
    print("***************************************************")


    
#Main Function
df = pd.read_csv('input-File.csv')

#Opening Browser
web = Browser()

#Getting contact URL link from the webbot
#Calling Function find_contact_page
df['contact_link'] = df.apply(lambda row: find_contact_page(row['Domain']), axis=1)


#Calling function "get_content" to scrape the contents
df['Contents'] = df.apply(lambda row: get_content(row['contact_link']), axis=1)


#stripping new line
df['Contents'] = df.apply(lambda row: str(row['Contents']).replace('\n',''), axis=1)

#stripping Douple whitespace
df['Contents'] = df.apply(lambda row: str(row['Contents']).replace('  ',''), axis=1)

#stripping comma
df['Contents'] = df.apply(lambda row: str(row['Contents']).replace(',',' '), axis=1)

#Output
df.to_csv('output_file.csv', index=False)